import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PDFToExcelConverter {

    public static void main(String[] args) {
        String pdfFilePath = "../PdfToExcel/target/mcaSyllabus.pdf"; // Replace with the path to your PDF file
        String excelFilePath = "../PdfToExcel/target/mca.xlsx"; // Replace with the desired output Excel file path

        try {
            // Load the PDF file
            PDDocument document = Loader.loadPDF(new File(pdfFilePath));

            // Create a new Excel workbook
            Workbook workbook = new XSSFWorkbook();

            // Iterate through each page in the PDF
            for (int pageNumber = 0; pageNumber < document.getNumberOfPages(); pageNumber++) {
                // Extract text from the current page
                PDFTextStripper pdfTextStripper = new PDFTextStripper();
                pdfTextStripper.setStartPage(pageNumber + 1); // Start from the current page
                pdfTextStripper.setEndPage(pageNumber + 1);   // End at the current page
                String text = pdfTextStripper.getText(document);

                // Create a new Excel sheet for the current page
                Sheet sheet = workbook.createSheet("Page " + (pageNumber + 1));

                // Split the extracted text into lines
                String[] lines = text.split("\n");
                int rowNum = 0;

                // Iterate through each line and add it to the Excel sheet
                for (String line : lines) {
                    Row row = sheet.createRow(rowNum++);
                    String[] values = line.split("\t"); // Assuming tab as a delimiter

                    for (int i = 0; i < values.length; i++) {
                        Cell cell = row.createCell(i);
                        cell.setCellValue(values[i]);
                    }
                }
            }

            // Write the Excel data to a file
            try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
                workbook.write(outputStream);
                System.out.println("PDF converted to Excel successfully!");
            }

            // Close the PDF document and Excel workbook
            document.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
